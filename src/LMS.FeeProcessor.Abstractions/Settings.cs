﻿
using System;

namespace LMS.FeeProcessor
{
    public class Settings
    {
        // private const string Prefix = "FEE-PROCESSOR";

        // public static string ServiceName { get; } = Prefix.ToLower();   
         public static string ServiceName => Environment.GetEnvironmentVariable($"CONFIGURATION_NAME") ?? "fee-processor";

    }
}
