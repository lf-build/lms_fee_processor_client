﻿using LendFoundry.Foundation.Client;
using LMS.LoanAccounting;
using Newtonsoft.Json;

namespace LMS.FeeProcessor.Abstractions
{
    public class AccrualDetails: IAccrualDetails
    {
        [JsonConverter(typeof(InterfaceConverter<IAccrualProcessRequest, AccrualProcessRequest>))]
        public IAccrualProcessRequest AccrualProcessRequest { get; set; }
        public bool IsStart { get; set; }
    }
}
