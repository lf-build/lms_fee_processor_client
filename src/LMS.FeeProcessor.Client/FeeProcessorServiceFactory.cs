﻿using LMS.FeeProcessor.Abstractions;
using System;
using LendFoundry.Security.Tokens;
using LendFoundry.Foundation.Client;
using LendFoundry.Foundation.Logging;
using Microsoft.Extensions.DependencyInjection;

namespace LMS.FeeProcessor.Client
{
    public class FeeProcessorServiceFactory : IFeeProcessorServiceFactory
    {
        public FeeProcessorServiceFactory(IServiceProvider provider, string endpoint, int port)
        {
            Provider = provider;
            Uri = new UriBuilder("http", endpoint, port).Uri;
        }
        public FeeProcessorServiceFactory(IServiceProvider provider, Uri uri = null)
        {
            Provider = provider;
            Uri = uri;
        }

        private IServiceProvider Provider { get; }
        private Uri Uri { get; }



        private string Endpoint { get; }

        private int Port { get; }

        public IFeeProcessorService Create(ITokenReader reader)
        {
            var uri = Uri;
            if (uri == null)
            {
                var logger = Provider.GetService<ILoggerFactory>().Create(NullLogContext.Instance);
                uri = Provider.GetRequiredService<IDependencyServiceUriResolverFactory>().Create(reader, logger).Get("fee_processor");
            }
            var client = Provider.GetServiceClient(reader, uri);
            return new FeeProcessorService(client);
        }
    }
}
