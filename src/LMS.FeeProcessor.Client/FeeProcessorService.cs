﻿using LMS.FeeProcessor.Abstractions;
using LMS.LoanManagement.Abstractions;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using RestSharp;
using System.Linq;
using LendFoundry.Foundation.Client;

namespace LMS.FeeProcessor.Client
{
    public class FeeProcessorService : IFeeProcessorService
    {

        public FeeProcessorService(IServiceClient client)
        {
            Client = client;
        }

        private IServiceClient Client { get; }

        public async Task<List<IFeeDetails>> BuildFeeSchedule(string loanNumber)
        {
            var request = new RestRequest("/{loanNumber}/buildfeeschedule", Method.GET);
            request.AddUrlSegment(nameof(loanNumber), loanNumber);
            var feeDetails = await Client.ExecuteAsync<List<FeeDetails>>(request);
            return feeDetails.ToList<IFeeDetails>();
        }

        public Task ProcessEvent(string loanNumber, string eventName, object data)
        {
            throw new NotImplementedException();
        }
    }
}
