﻿using LMS.FeeProcessor.Abstractions;
using LendFoundry.Security.Tokens;

namespace LMS.FeeProcessor.Client
{
    public interface IFeeProcessorServiceFactory
    {
        IFeeProcessorService Create(ITokenReader reader);
    }
}
